const figlet = require('figlet');
const prompts = require('prompts');
const json2md = require('json2md')
const request = require('request');
const fs = require('fs');
const { resolve } = require('path');

const createTestCase = false;
var textCfg = {
  required: "M"
};

let config_json = []


let mdAArray = []

const getTitle = (object) => object.func.query_string['func'].desc ? object.func.query_string['func'].desc : ''

const getDescription = (object) => object.func.service_desc ? object.func.service_desc : ''

const getUrl = (object) => {
  path = object.func.path + '?'
  for (const [key, value] of Object.entries(object.func.query_string))
    path += key + '=' + value.val + '&&'
  return path.substring(0, path.length - 2);
}

const listParams = (object) => {
  for (const [key, value] of Object.entries(object.func.query_string))
    mdAArray.push( `* ${key} (${value.is_require ? textCfg.required : ''})  ${'\n  - ' + value.desc} ${value.format ? '\n  - Format : ' + value.format : ''} \n`)

}

const basic_question = [
  {
    type: 'text',
    name: 'username',
    message: 'Username : '
  },
  {
    type: 'text',
    name: 'password',
    message: 'Password : '
  }
];
const token_question = [
  {
    type: 'text',
    name: 'reply',
    message: 'Token Giriniz'
  }
];
const filename_question = [
  {
    type: 'text',
    name: 'reply',
    message: 'Filename : '
  }
];
const filepath_question = [
  {
    type: 'text',
    name: 'path',
    message: 'Filepath : '
  }
];

const getResponse = (object) => {
  return new Promise(async (res, rej) => {
    let auth_response = ''
    switch (object.func.auth_type) {
      case 'Basic':
        if (!object.func.auth_info)
          auth_response = await prompts(basic_question);
        else {
          let username = object.func.auth_info.username
          let password = object.func.auth_info.password
          console.log('Basic auth setted username : ',username,' password : ',password)
          //set basic auth 
        }
        break;
      case 'Token':
        if (!object.func.auth_info)
          auth_response = await prompts(token_question);
        else { 
          let token = object.func.auth_info.token
           
          console.log('Token setted ',token)
          //set token
        }
        break;
      default:
        console.log('There is no auth')
        break;
    }

    if (object.func.req_method == 'GET') {
      request(getUrl(object), function (error, response, body) {
        res(body || '')
      });
    }
    else if (object.func.req_method == 'POST') {
      request.post(getUrl(object),
        { form: object.func.body_string.body },
        function (err, httpResponse, body) { res(body || '') })
    }
    else res('Unsupported request method')
  })
}
const read_file = async(filepath) =>
  new Promise((res,rej)=>{
    fs.readFile(filepath.trim(), function read(err, data) {
      if (err) {
        res([])
      }
      const content = data;
      res(JSON.parse(content.toString()))
    });
  })
   

const make_a_readme = async () => {
   
  let filepath = await prompts(filepath_question)

  let filename = await prompts(filename_question)
  
  config_json = await read_file (filepath.path)
  console.log(config_json)
  for (let i = 0; i < config_json.length; i++) {

    mdAArray.push({h2:getTitle(config_json[i])+'\n'} )
    mdAArray.push( getDescription(config_json[i])+'\n')
    let response = await getResponse(config_json[i])

    mdAArray.push( 'Request parameters:\n' )

    listParams(config_json[i])

    mdAArray.push({
      code: {
        language: 'markdown', content: [
          `Request URL: http://{host}:{port}/${getUrl(config_json[i])}`,
          `Response: ${response}`
        ]
      }
    })
    mdAArray.push( `Auth Type : ${config_json[i].func.auth_type ? config_json[i].func.auth_type : '-'}\n` )
  }
  console.log(json2md(mdAArray))
  fs.writeFile(filename.reply, json2md(mdAArray), (err) => {
    if (err) console.log(err)
    console.log('File created succesfully.')
  })
}
make_a_readme()

// console.log(json2md(object))

// const questions = [
//   {
//     type: 'select',
//     name: 'testIt',
//     message: 'Would you like create the end to ent test file ?',
//     choices: [
//       { title: 'Always yes 🌠 ', value: true },
//       { title: 'i test it in production 🌈', value: false }
//     ],
//   },
//   {
//     type: 'text',
//     name: 'reply',
//     message: prev => prev ? 'clone the repo and start to write 🐌' : 'You are the champion 💩'
//   }
// ];

// figlet('API - DOC Gen.', function (err, data) {
//   if (err) { console.log('figlet went wrong...'); return; }
//   console.log(data);
//   createTestCase = (async () => {
//     const response = await prompts(questions);
//     console.log('response', response);
//    
//     console.log(json2md(object))
//     return response.testIt;
//   })();
// });


